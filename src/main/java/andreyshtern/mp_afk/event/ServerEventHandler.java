package andreyshtern.mp_afk.event;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.sync.AfkManager;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class ServerEventHandler {
    @SubscribeEvent
    public void playerJoin(PlayerEvent.PlayerLoggedInEvent e){
        if(MpAfk.SERVER) {
            AfkManager.resetTimerForPlayer(e.player);
        }
    }
    @SubscribeEvent
    public void playerLeft(PlayerEvent.PlayerLoggedOutEvent e){
        if(MpAfk.SERVER){
            AfkManager.playerLeft(e.player);
        }
    }
    @SubscribeEvent
    public void serverTick(TickEvent.ServerTickEvent e){
        if(MpAfk.SERVER){
            if(e.phase == TickEvent.Phase.END)
                return;
            AfkManager.addTime();
            AfkManager.kickAfk();
        }
    }
}
