package andreyshtern.mp_afk.event;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.lang.Messages;
import andreyshtern.mp_afk.network.MPSServerPackets;
import andreyshtern.mp_afk.sync.SendManager;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;


public class ClientEventHandler {

    @SubscribeEvent
    public void sendBuffer(TickEvent.ClientTickEvent event){
        GameSettings keybinds = Minecraft.getMinecraft().gameSettings;
        for(KeyBinding kb : keybinds.keyBindings){
            if(kb.getIsKeyPressed())
                SendManager.addMotionToBuffer();
        }
        if(Minecraft.getMinecraft().isGamePaused() && Minecraft.getMinecraft().thePlayer == null && event.phase == TickEvent.Phase.END){
            return;
        }
        SendManager.timerTick();
        SendManager.tryToSend();
    }

}
