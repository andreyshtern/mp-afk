package andreyshtern.mp_afk.network;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.sync.AfkManager;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import io.netty.buffer.ByteBuf;
import keelfy.klibrary.network.KPacketReceiver;

public enum  ServerPacketHandler {
    INSTANCE;
    public static void register(){
        MpAfk.kNetwork.registerPacketHandler(INSTANCE);
    }
    @SubscribeEvent
    public void onServerPacket(final FMLNetworkEvent.ServerCustomPacketEvent event) {
        if(MpAfk.SERVER) {
            final KPacketReceiver.ServerPacketReceiver receiver;

            receiver = new KPacketReceiver.ServerPacketReceiver(event);
            final MPSServerPackets packet = MPSServerPackets.values()[receiver.getPacketNumber()];
            final ByteBuf buffer = receiver.getBuffer();
            switch (packet) {
                case ACTION_PACKET:
                    AfkManager.resetTimerForPlayer(receiver.getPlayer());
                    break;
                case SEND_KICK:
                    AfkManager.setKickMessage(receiver.getPlayer(), ByteBufUtils.readUTF8String(buffer));
                    break;

            }
        }

    }
}
