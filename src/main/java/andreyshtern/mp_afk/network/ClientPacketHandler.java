package andreyshtern.mp_afk.network;

import andreyshtern.mp_afk.MpAfk;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import io.netty.buffer.ByteBuf;
import keelfy.klibrary.network.KPacketReceiver;

public enum  ClientPacketHandler {
    INSTANCE;
    public static void register(){
        MpAfk.kNetwork.registerPacketHandler(INSTANCE);
    }
    @SubscribeEvent
    public void onClientPacket(final FMLNetworkEvent.ClientCustomPacketEvent event) {
        final KPacketReceiver.ClientPacketReceiver receiver;
        try {
            receiver = new KPacketReceiver.ClientPacketReceiver(event);

        } catch (NegativeArraySizeException e){
            return;
        }
        final MPSClientPackets packet = MPSClientPackets.values()[receiver.getPacketNumber()];
        final ByteBuf buffer = receiver.getBuffer();
        if (receiver.getPlayer() == null)
            return;

        switch (packet) {



        }
    }

}
