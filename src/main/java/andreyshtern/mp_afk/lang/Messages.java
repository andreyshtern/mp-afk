package andreyshtern.mp_afk.lang;

import andreyshtern.mp_afk.MpAfk;
import keelfy.mppermit.server.utils.IMessagesHandler;
import net.minecraft.command.ICommandSender;
import org.apache.logging.log4j.Logger;

public enum Messages implements IMessagesHandler {
    AFK;

    private String code;
    private String serverMessage;

    private Messages() {
        this("");
    }

    private Messages(String serverMessage) {
        this.code = MpAfk.MODID + ".msg." + this.toString().toLowerCase();
        this.serverMessage = serverMessage;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getDefaultMessage() {
        return this.serverMessage;
    }

    @Override
    public Logger getLogger() {
        return MpAfk.LOGGER;
    }
}
