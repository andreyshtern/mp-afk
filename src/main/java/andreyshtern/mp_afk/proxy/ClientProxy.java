package andreyshtern.mp_afk.proxy;

import andreyshtern.mp_afk.event.ClientEventHandler;
import andreyshtern.mp_afk.event.ServerEventHandler;
import andreyshtern.mp_afk.network.ClientPacketHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {
    public void preInit(FMLPreInitializationEvent event){
        super.preInit(event);
    }
    public void init(FMLInitializationEvent event){
        super.init(event);
        ClientPacketHandler.register();
        FMLCommonHandler.instance().bus().register(new ClientEventHandler());
    }
    public void postInit(FMLPostInitializationEvent event){
        super.postInit(event);
    }
}
