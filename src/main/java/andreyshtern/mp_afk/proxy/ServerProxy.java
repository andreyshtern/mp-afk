package andreyshtern.mp_afk.proxy;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.config.ConfigManager;
import andreyshtern.mp_afk.event.ServerEventHandler;
import andreyshtern.mp_afk.network.ServerPacketHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ServerProxy extends CommonProxy {
    public void preInit(FMLPreInitializationEvent event){
        super.preInit(event);
        ConfigManager.initConfig(event);
    }
    public void init(FMLInitializationEvent event){
        super.init(event);
        if(MpAfk.SERVER){
            ServerPacketHandler.register();
            FMLCommonHandler.instance().bus().register(new ServerEventHandler());
        }

    }
    public void postInit(FMLPostInitializationEvent event){
        super.postInit(event);
    }
}
