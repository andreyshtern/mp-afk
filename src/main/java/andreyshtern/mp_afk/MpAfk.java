package andreyshtern.mp_afk;

import andreyshtern.mp_afk.proxy.CommonProxy;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import keelfy.klibrary.network.KNetwork;
import net.minecraft.init.Blocks;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = MpAfk.MODID, version = MpAfk.VERSION)
public class MpAfk
{
    public static final String MODID = "mp_afk";
    public static final String VERSION = "1.0";
    public static KNetwork kNetwork;
    public static Logger LOGGER = FMLLog.getLogger();
    public static final boolean SERVER = true;
    @SidedProxy(clientSide = "andreyshtern.mp_afk.proxy.ClientProxy", serverSide = "andreyshtern.mp_afk.proxy.ServerProxy")
    private static CommonProxy proxy;
    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        proxy.preInit(event);
    }
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        kNetwork = new KNetwork(MpAfk.MODID);
		proxy.init(event);
    }
    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        proxy.postInit(event);
    }
}
