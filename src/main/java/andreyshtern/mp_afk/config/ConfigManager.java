package andreyshtern.mp_afk.config;

import andreyshtern.mp_afk.MpAfk;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.common.config.Configuration;


import java.io.IOException;

public class ConfigManager {
    private static int delay;

    public static void initConfig(FMLPreInitializationEvent e) {
        if(!e.getSuggestedConfigurationFile().exists()){
            try {
                e.getSuggestedConfigurationFile().createNewFile();
            } catch (IOException exc){
                MpAfk.LOGGER.error("SHIT! I can't create config file!");
                exc.printStackTrace();
            }
        }
        Configuration config = new Configuration(e.getSuggestedConfigurationFile());
        config.load();
        delay = config.getInt("afkDelayMin","general",10,1,1440,"Delay (in minutes) after AFK player will be kicked.");
        config.save();
    }

    public static int getDelay() {
        return delay;
    }

    public static int getDelayInTicks(){
        return delay * 60 * 20;
    }
}
