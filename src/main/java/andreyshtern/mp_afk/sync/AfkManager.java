package andreyshtern.mp_afk.sync;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.config.ConfigManager;
import andreyshtern.mp_afk.lang.Messages;
import keelfy.klibrary.server.KServerUtils;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

import java.util.HashMap;

public class AfkManager {
    private static HashMap<EntityPlayer, Integer> countdown = new HashMap<EntityPlayer, Integer>();
    private static HashMap<EntityPlayer, String> kickMessages = new HashMap<EntityPlayer, String>();
    public static void setKickMessage(EntityPlayer player, String message){
        if(MpAfk.SERVER) {
            kickMessages.put(player, message);
        }
    }
    public static void addTime(){
        if(MpAfk.SERVER){
            for (EntityPlayer player : countdown.keySet()){
                countdown.put(player,countdown.get(player)+1);
            }
        }
    }

    public static void resetTimerForPlayer(EntityPlayer player){
        countdown.put(player, 0);
    }

    public static void kickAfk(){
        if(MpAfk.SERVER){
            for(EntityPlayer player : countdown.keySet()){
                int afkTime = countdown.get(player);
                if(afkTime > ConfigManager.getDelayInTicks()){
                    ((EntityPlayerMP) player).playerNetServerHandler.kickPlayerFromServer(kickMessages.get(player));
                    playerLeft(player);
                }
            }
        }
    }
    public static void playerLeft(EntityPlayer player){
        if(MpAfk.SERVER) {
            countdown.remove(player);
        }
    }
}
