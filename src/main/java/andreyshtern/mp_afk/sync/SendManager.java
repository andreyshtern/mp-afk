package andreyshtern.mp_afk.sync;

import andreyshtern.mp_afk.MpAfk;
import andreyshtern.mp_afk.network.MPSServerPackets;
import net.minecraft.client.resources.I18n;

public class SendManager {
    private static int timer = 0;
    private static int buffer = 0;


    public static void resetTimer() {
        timer = 0;
    }
    public static void timerTick() {
        timer++;
        if(timer >= 20){
            MpAfk.kNetwork.sendToServer(MPSServerPackets.SEND_KICK, I18n.format("mp_afk.kick"));
        }
    }
    public static void addMotionToBuffer(){
        buffer++;
    }
    public static void resetBuffer(){
        buffer = 0;
    }
    public static void tryToSend(){
        if(buffer > 0 && timer >= 20){
            MpAfk.kNetwork.sendToServer(MPSServerPackets.ACTION_PACKET);

            resetTimer();
            resetBuffer();
        }
    }
}
